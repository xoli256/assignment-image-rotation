#ifndef IMAGE
#define IMAGE

#include <stdint.h>

struct  __attribute__((__packed__)) pixel { uint8_t b, g, r; };
// not incomplete because we may need to access pixels rgb directly in other image transformations
struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image image_malloc( uint32_t height, uint32_t width );
void image_free( struct image* img );
void pixel_set( struct image* img, uint32_t x, uint32_t y, struct pixel pixel );

#endif
