#ifndef ROTATE
#define ROTATE

#include "bmp_serialization.h"
#include "image.h"
enum edit_status image_rotate_counterclockwise_90 ( struct image const source, struct image* out);
#endif
