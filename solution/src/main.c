#include "bmp_serialization.h"
#include "rotate.h"

#define ARGS_VALIDATING_ERROR_MESSAGES_COUNT 4
enum args_validation_status  {
    VALIDATION_OK = 0,
    VALIDATION_NOT_ENOUGH_ARGS,
    VALIDATION_INVALID_SOURCE_FILE,
    VALIDATION_INVALID_RESULT_FILE
};

char* args_validating_error_messages[ARGS_VALIDATING_ERROR_MESSAGES_COUNT] = {
        [VALIDATION_NOT_ENOUGH_ARGS] = "Not enough arguments",
        [VALIDATION_INVALID_SOURCE_FILE] ="Couldn't open source file (argument 1) on read",
        [VALIDATION_INVALID_RESULT_FILE] ="Couldn't open target file (argument 2) on write"
};

char* reading_error_messages[READING_ERROR_MESSAGES_COUNT] = {
        [READ_INVALID_BITS] = "couldn't read pixels",
        [READ_INVALID_HEADER] = "couldn't read header",
        [READ_INVALID_OFFSET] = "failed while skipping offset",
        [READ_INVALID_PADDING] = "failed while skipping padding",
        [READ_OUT_OF_MEMORY] = "couldn't allocate image"
};

char* editing_error_messages[EDITING_ERROR_MESSAGES_COUNT] = {
        [EDIT_OUT_OF_MEMORY] = "couldn't allocate image"
};

char* writing_error_messages[WRITING_ERROR_MESSAGES_COUNT] = {
        [WRITE_HEADER_ERROR] ="failed while writing header",
        [WRITE_BODY_ERROR] = "failed while writing pixels"
};

static inline enum args_validation_status validate_args( int argc, char** argv, FILE** fin, FILE** fout ) {
    if ( argc < 3 ) return VALIDATION_NOT_ENOUGH_ARGS;
    if ( argc > 3 ) printf("Notice: too many arguments, %d last arguments ignored\n", argc - 3);

    *fin = fopen(argv[1], "rb");
    if ( NULL == *fin ) return VALIDATION_INVALID_SOURCE_FILE;

    *fout = fopen(argv[2], "wb");
    if ( NULL == *fout ) return VALIDATION_INVALID_RESULT_FILE;

    return 0;
}

int main( int argc, char** argv ) {
    FILE* fin;
    FILE* fout;

    enum args_validation_status av_status = validate_args( argc, argv, &fin, &fout );
    if ( VALIDATION_OK != av_status ) {
        printf("%s", args_validating_error_messages[av_status]);
        if (av_status != VALIDATION_NOT_ENOUGH_ARGS && av_status != VALIDATION_INVALID_SOURCE_FILE) fclose(fin);
        return 1;
    }

    struct image img = {0};
    enum read_status r_status = from_bmp(fin, &img);
    if ( READ_OK != r_status ) {
        printf("Couldn't read image from %s: %s\n" , argv[1], reading_error_messages[r_status]);
        if (r_status != READ_OUT_OF_MEMORY) image_free(&img);
        fclose(fin);
        return 2;
    }
    fclose(fin);

    struct image transformed;
    enum edit_status e_status = image_rotate_counterclockwise_90(img, &transformed);
    if ( EDIT_OK != e_status ) {
        printf("Couldn't read image from %s: %s\n" , argv[1], reading_error_messages[r_status]);
        if (e_status != EDIT_OUT_OF_MEMORY) image_free(&transformed);
        image_free(&img);
        return 3;
    }
    image_free(&img);

    enum write_status w_status = to_bmp(fout, &transformed);
    if ( WRITE_OK != w_status ) {
        printf("Couldn't write image to %s: %s\n" , argv[2], writing_error_messages[w_status]);
        image_free(&transformed);
        fclose(fout);
        return 4;
    }

    image_free(&transformed);
    fclose(fout);
    return 0;
}
