#include "rotate.h"

enum edit_status image_rotate_counterclockwise_90 ( struct image const source, struct image* out) {
    *out = image_malloc(source.width, source.height);
    if (out -> data == NULL) return EDIT_OUT_OF_MEMORY;
    for( uint32_t y = 0; y < (source.height); y++ ) {
        for ( uint32_t x = 0; x < (source.width); x++ ) {
            pixel_set( out, (source.height - 1) - y, (source.width - 1) - ( ( (source.width) - 1 ) - x ), source.data[(source.width) * y + x] );
        }
    }
    return EDIT_OK;
}
