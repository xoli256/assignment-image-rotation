#include "bmp_serialization.h"
#include <inttypes.h>

#define BMP_SIGNATURE 0x4D42    // is hex for "BM" - specifies that image is BMP
#define BIT_PER_COLOR 24
#define PLANES 1
#define PIXEL_PER_M 2834

struct __attribute__((__packed__)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

static inline size_t bmp_header_read( FILE* in, struct bmp_header* header ) {
    return fread(header, sizeof(struct bmp_header), 1, in);
}

static inline uint32_t padding_calculate( uint32_t image_width ) {
    return (4 - (sizeof(struct pixel) * image_width) % 4) % 4;
}

static inline int skip_offset( FILE* in, uint32_t offset ) {
    return fseek(in, offset, SEEK_SET);
}

static inline size_t bmp_body_read_row( FILE* in, struct image* img, uint32_t row ) {
    return fread( (img -> data) + (img -> width) * row, sizeof(struct pixel), img -> width, in );
}

static inline int skip_padding( FILE* in, uint32_t padding ) {
    return fseek(in, padding, SEEK_CUR);
}

static inline enum read_status bmp_body_read( FILE* in, struct image* img, uint32_t padding ) {
    for ( uint32_t i = 0; i < (img -> height); i++ ) {

        if ( bmp_body_read_row( in, img, i ) != img -> width ) {
            return READ_INVALID_BITS;
        }

        if ( skip_padding( in, padding ) != 0 ) {
            return READ_INVALID_PADDING;
        }
    }
    return READ_OK;
}

enum read_status from_bmp( FILE* in, struct image* img ) {
    struct bmp_header header = {0};

    if ( bmp_header_read( in, &header ) < 1 ) {
        return READ_INVALID_HEADER;
    }

    *img = image_malloc( header.biHeight, header.biWidth );
    if ( NULL == (img -> data) ) {
        return READ_OUT_OF_MEMORY;
    }

    if ( skip_offset(in, header.bOffBits) != 0 ) {
        return READ_INVALID_OFFSET;
    }

    uint32_t padding = padding_calculate(img -> width);
    return bmp_body_read(in, img, padding);
}


static inline size_t write_header( FILE* out, struct bmp_header* header ) {
    return fwrite(header, sizeof(struct bmp_header), 1, out);
}

static inline struct bmp_header bmp_header_create( uint32_t height, uint32_t width ) {
    size_t image_size = height * (width + padding_calculate(width)) * sizeof(struct pixel);
    return (struct bmp_header) {
            .bfType = BMP_SIGNATURE,
            .biHeight = height,
            .biWidth = width,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biPlanes = PLANES,
            .bfileSize = sizeof(struct bmp_header) + image_size,
            .biCompression = 0,
            .bfReserved = 0,
            .biBitCount = BIT_PER_COLOR,
            .biXPelsPerMeter = PIXEL_PER_M,
            .biYPelsPerMeter = PIXEL_PER_M,
            .biClrUsed = 0,
            .biClrImportant = 0,
            .biSizeImage = image_size
    };
}

static inline size_t bmp_body_write_row( FILE* out, struct image const* img, uint32_t row ) {
    return fwrite( &img -> data[ row * (img -> width) ], sizeof (struct pixel), img -> width, out );
}

enum write_status to_bmp( FILE* out, struct image const* img ) {

    struct bmp_header header = bmp_header_create( img -> height, img -> width);

    if ( write_header( out, &header ) < 1 ) {
        return WRITE_HEADER_ERROR;
    }

    size_t padding = padding_calculate( img -> width );

    for ( uint32_t row = 0; row < (img -> height); row++ ) {

        if ( bmp_body_write_row( out, img, row )  != img -> width ) {
            return WRITE_BODY_ERROR;
        }

        for ( size_t i=0; i < padding; i++ ){
            putc( 0, out );
        }
    }
    return WRITE_OK;
}
